<?php
/**
 * Zend Developer Tools for Zend Framework (http://framework.zend.com/)
 *
 * @link       http://github.com/zendframework/ZfjNavigation for the canonical source repository
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd New BSD License
 * @package    ZfjNavigation
 * @subpackage Exception
 */

namespace ZfjNavigation\Exception;

/**
 * @category   Zend
 * @package    ZfjNavigation
 * @subpackage Exception
 */
class ParameterMissingException extends \Exception implements ExceptionInterface
{

}