<?php
namespace ZfjNavigation;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Nette\Diagnostics\Debugger;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use ZfjNavigation\View\Helper\Navigation;

class Module implements
    ConfigProviderInterface,
    AutoloaderProviderInterface,
    ServiceProviderInterface,
    ViewHelperProviderInterface
{
	
	
    /**
     * {@InheritDoc}
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    /**
     * {@InheritDoc}
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }
    
    /**
     * @inheritdoc
     */
    public function getServiceConfig()
    {
    	return array(
    		'aliases' => array(
    			'ZfjNavigation\ReportInterface' => 'ZfjNavigation\Report',
    			'zfj_navigation_doctrine_em' => 'Doctrine\ORM\EntityManager'
    		),
    		'invokables' => array(
    			'ZfjNavigation\Report'             => 'ZfjNavigation\Report',
    			'ZfjNavigation\EventCollector'     => 'ZfjNavigation\Collector\EventCollector',
    			'ZfjNavigation\ExceptionCollector' => 'ZfjNavigation\Collector\ExceptionCollector',
    			'ZfjNavigation\RouteCollector'     => 'ZfjNavigation\Collector\RouteCollector',
    			'ZfjNavigation\RequestCollector'   => 'ZfjNavigation\Collector\RequestCollector',
    			'ZfjNavigation\ConfigCollector'    => 'ZfjNavigation\Collector\ConfigCollector',
    			'ZfjNavigation\MailCollector'      => 'ZfjNavigation\Collector\MailCollector',
    			'ZfjNavigation\MemoryCollector'    => 'ZfjNavigation\Collector\MemoryCollector',
    			'ZfjNavigation\TimeCollector'      => 'ZfjNavigation\Collector\TimeCollector',
    			'ZfjNavigation\FlushListener'      => 'ZfjNavigation\Listener\FlushListener',
    			'ZfjNavigation\Collector\UriCollector' => 'ZfjNavigation\Collector\UriCollector'
    		),
    		'factories' => array(
    			'ZfjNavigation\Profiler' => function ($sm) {
    				$a = new Profiler($sm->get('ZfjNavigation\Report'));
    				$a->setEvent($sm->get('ZfjNavigation\Event'));
    				return $a;
    			},
    			'ZfjNavigation\Config' => function ($sm) {
    				$config = $sm->get('Configuration');
    				$config = isset($config[__NAMESPACE__]) ? $config[__NAMESPACE__] : null;
    				
    				return new Options($config, $sm->get('ZfjNavigation\Report'));
    			},
    			'ZfjNavigation\Event' => function ($sm) {
    				$event = new ProfilerEvent();
    				$event->setReport($sm->get('ZfjNavigation\Report'));
    				$event->setApplication($sm->get('Application'));
    
    				return $event;
    			},
    			'ZfjNavigation\StorageListener' => function ($sm) {
    				return new Listener\StorageListener($sm);
    			},
    			'ZfjNavigation\ToolbarListener' => function ($sm) {
    				return new Listener\ToolbarListener($sm->get('ViewRenderer'), $sm->get('ZfjNavigation\Config'));
    			},
    			'ZfjNavigation\ProfilerListener' => function ($sm) {
    				return new Listener\ProfilerListener($sm, $sm->get('ZfjNavigation\Config'));
    			},
    			'ZfjNavigation' => 'ZfjNavigation\Navigation\NavigationFactory'
    		),
    	);
    }
    
    public function getViewHelperConfig()
    {
    	return array(
    		'factories' => array(
    			'ZfjNavigation' => function($sm, $s) {
    				$navigation = new Navigation();
    				return $navigation;
    			}
    		)
    	);
    }
}
